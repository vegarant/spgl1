#include "spgl1Algorithm.h"



/*

The main solver of the spgl1 algorithm. Searches for the solution of 

               minimize ||z||_1 subject to ||Az-b||_2 ≤ sigma 

INPUT:
ft    - A fast transform, representing the matrix A.
b     - Measurements.
sigma - Accuracy
LASSOMaxItr - Maximum number of iterations used in the LASSO algorithm.

*/
template <typename T>
T* Spgl1Algorithm::rootFinding(FastTransformBase<T>& ft, T* b,  double sigma, int LASSOMaxItr) {
     
    const double eps = 1e-8;
     
    const size_t N = ft.cols();
    const size_t M = ft.rows();
    
    double *x_tau = new double[N];
    double *r_tau = new double[M];
     
    std::memset(x_tau,0, sizeof(T)*N);
    std::memcpy(r_tau, b, sizeof(T)*M);
     
    std::vector<T> tau_vector;
    tau_vector.push_back(0);
     
    double r_norm2 = norm2<T>(r_tau, M);
     
    double delta = std::abs((r_norm2 - sigma)/sigma);
     
    int i = 0;
     
    T *result = new T[N];
    
    T *residual = new T[M];
    std::cout << "||b||_2: " << norm2<T>(b,M) << std::endl;
     
    while (r_norm2 >= sigma ) {//i < 7 or tau_vector.back() > 400) {
         
        std::cout << "************* Root iteration: " << i <<" ****************" 
                                                                   << std::endl; 
        //***************** Run the actual algorithm **************** 
        std::cout << "Delta: " << delta << std::endl;
        LASSO_approx(ft, b, tau_vector.back(), delta,  x_tau, r_tau, LASSOMaxItr); 
        r_norm2 = norm2<T>(r_tau, M);  
         
        for (int i = 0; i < M; i++) {
            r_tau[i] = r_tau[i]/r_norm2;  
        }
         
        // ||A^T (r/||r||_2) ||_∞
        ft(r_tau, result, 1.0, true); 
         
        double lambda_tau = normInfty<T>(result, N); 
         
        double delta_tau = (sigma - r_norm2)/(-lambda_tau);
        double tau_current = tau_vector.back(); 
        tau_vector.push_back(tau_current + delta_tau);
         
        double x_tau_norm2 = norm2<T>(x_tau, N);
         
        i++;
        std::cout << i << ": tau: " << tau_vector.back() << std::endl;
        std::cout << i << ": ||x_tau||_2: " << x_tau_norm2  << std::endl;
        std::cout << i << ": Δτ: " << delta_tau << ", σ: " << sigma<< std::endl;
        std::cout << i << ": ||r_norm2||_2: " << r_norm2 << ", ϕ'(τ): "
                  << -lambda_tau << std::endl;   
         
        //********************* Test if ||r||_2 == ||Ax-b||_2 **********************
        std::memset(residual, 0, sizeof(T)*M);
        // residual <- A x_{tau}
        ft(x_tau, residual, 1.0, false);
        
        for (int i = 0; i < M; i++) {
            residual[i] = b[i] - residual[i];
        }
         
        double residual_norm2 = norm2<T>(residual, M); 
        
        if (std::abs(residual_norm2 - r_norm2) > eps) {
            std::cout << "\e[32m residual_norm2: " << residual_norm2 << ", ||r||_2: "
                      << r_norm2 << "\e[0m" << std::endl;
        }

        //***************************** End test *******************************
        
        
        // Compute the new duality gap, based on the accuracy of the previous 
        // solution. See section 5 of the "Probing the Pareto frontier for BP 
        // solutions" article.   
        delta = std::abs((sigma - r_norm2)/sigma); 
    }
     
    for (int i = 0; i < tau_vector.size(); i++) {
        std::cout << std::setw(3) << i << " tau: " << tau_vector[i] << std::endl;
    } 
    
    delete [] r_tau;
    delete [] residual;
    delete [] result;
    
    return x_tau;
}


template double* Spgl1Algorithm::rootFinding<>(FastTransformBase<double>& ft, 
                                  double* b,  double sigma, int LASSOMaxItr);



/*

ft    - The fast transform  
b     - The measurement vector
tau   - Minimize || Ax - b||_2  subject to ||x||_1 ≤ tau
delta - Duality-gap
x     - Initial solution. The final solution will be returned using this pointer
r     - Residual. It is assumed this pointer points to a memory area of size 
        M*sizeof(T). The residual will be returned through this pointer.  

*/
template <typename T>
void Spgl1Algorithm::LASSO_approx( FastTransformBase<T>& ft, T* b, double tau, double delta, T *x, T* r, int maxItr, bool verbose) {
    
    // step length
    double alpha_ell = alphaMin + (alphaMin + alphaMax)/2; 
     
    const size_t N = ft.cols();
    const size_t M = ft.rows();
     
    // T *x = new T[N];
    // T *r = new T[M];
    T *g = new T[N];
    T *x_bar = new T[N];
    T *r_bar = new T[N]; // This value should presumably be M
    T *g_old = new T[N];
    // x_0 = P_τ[x]
    
    projectNorm1<T>(x, N, tau);
    //std::cout << "||x_0||_2: "<< norm2<T>(x, N) << std::endl;
    
    // r = b - A*x_0
    ft(x, r);
    //std::cout << "||ft(x,r)||_2: " <<  norm2<T>(r, M)<< std::endl;
    
    double max_r_sq_value = 0; // l2 norm squared
    for (int i = 0; i < M; i++) {
        r[i] = b[i] - r[i];
        max_r_sq_value += std::abs(r[i]*r[i]); // std::abs to handle complex numbers
    }
     
     
    std::vector<double> max_r_sq_vector;
    max_r_sq_vector.push_back(max_r_sq_value);
    
    ft(r,g,-1,true);
    
    

    int ell = 0;
    
    // Make the algorithm go into the while loop the first time
    double delta_ell = delta + 1;
    double delta_ell_prev = delta_ell;
    //*********************************************************************
    //***                    START OF MAIN LOOP                         ***
    //*********************************************************************
    
    bool hasConverged = false;
    while (ell < maxItr) {//( std::abs(delta_ell_prev - delta_ell) < 1e-5) {
        if (not verbose) {
            std::cerr << "\r";
            std::cerr << "ℓ: " << ell;
        }
        delta_ell_prev = delta_ell; 
        
        double norm2_r = norm2<T>(r, M);
        double dot_br = dot<T>(b,r,M);
        double normInftyG = normInfty<T>(g,N);
        //std::cout << "1: ||r||_2: "<< norm2_r << ", b^T*r: " << dot_br 
        //          << " ||g||_inf: " << normInftyG << std::endl; 
        //std::cout << "Tau: " << tau << "||r||_2 - b*r: " << norm2_r*norm2_r - dot_br<< std::endl;

        delta_ell = norm2_r - (dot_br - tau*normInftyG)/norm2_r;
        
        if (verbose) {
            std::cout <<"ell: "<< std::setw(3) << ell << ", delta_ell: " 
                      << delta_ell << ", ||r||_2: " << norm2_r << std::endl;
        }

        if (delta_ell < delta) {
            if (not verbose){
                std::cerr << "\r";
            }
            std::cout << "break: has converged. ℓ: " << ell << std::endl;
            hasConverged = true;
            break;    
        }
         
        double alpha = alpha_ell;
         
        bool continueLineSearch = true;
         
        int lineSearchCnt = 1;
        
        //*****************************************************************
        //***                 START OF LINE SEARCH                      ***
        //*****************************************************************
        
        //std::cout << "************** Line Search *****************" << std::endl;
        while (continueLineSearch and lineSearchCnt < lineSearchHistoryLength) {
             
            // x_bar = x_ell - alpha*g_ell
            for (int i = 0; i < N; i++) {
                x_bar[i] = x[i] - alpha*g[i];
            }
             
            // P_tau [ x_ell - alpha*g_ell ]
            projectNorm1<T>(x_bar, N, tau);        
            
            // A*x_bar
            ft(x_bar, r_bar); 
             
            // r_bar = b - A*x_bar
            for (int i = 0; i < M; i++) {
                r_bar[i] = b[i] - r_bar[i];
            }
             
            //*************************************************************
            //***        Check if we shall make a new iteration         ***
            //*************************************************************
            
            double r_bar_norm2 = norm2<T>(r_bar, M); 
            double r_bar_norm2_sq = r_bar_norm2*r_bar_norm2; 
            // Note: This dot product might not be positive, nor real valued
            // Possible debugg point 
            T dot_sum = 0;
            T debugSum = 0;
            for (int i = 0; i < N; i++) {
                dot_sum += (x_bar[i] - x[i])*g[i];
                debugSum += std::abs(x_bar[i] - x[i]);
            } 
            
            // NOTE: the min variables in this expression is inserted with a 
            // great uncertainty. 
            double max_r_ell_sq = findMaxElementInVector(max_r_sq_vector, lineSearchCnt, lineSearchHistoryLength-1); 
            

            if (verbose) {
                std::cout << std::setw(3) << lineSearchCnt  <<", dot_sum: " 
                          << dot_sum  << ", max_r: " << max_r_ell_sq 
                          << ", r_bar_norm2: " << r_bar_norm2_sq
                          << ", debugSum: " << debugSum << std::endl; 
                
            }
            continueLineSearch = r_bar_norm2_sq <= std::abs(max_r_ell_sq + gamma*dot_sum);
            lineSearchCnt++;                     
            alpha = alpha/2; 
        }
         
        //std::cout << "********************************************" << std::endl;
        //std::cout << "lineSearchCnt: " << lineSearchCnt << std::endl; 
        
        //***************************************************************** 
        //***                    End of line search                     ***
        //***                    Update all arrays                      ***
        //***************************************************************** 
         
        // Possible improvement: swap pointers 
         
        // r_{ell+1} = r_bar
        double max_r_value = norm2<T>(r_bar, M); // l2 norm squared
        
        max_r_sq_vector.push_back(max_r_value*max_r_value);
        std::memcpy(r, r_bar, sizeof(T)*M);
        
        // g_old = g
        std::memcpy(g_old, g, sizeof(T)*N);
         
        // g = -A^T*r
        ft(r, g, -1, true);
         
        T dot_delta_xg = 0;
        T dot_delta_xx = 0;
        T delta_g = 0;  
        T delta_x = 0; 
        
        // delta_x*delta_g and delta_x*delta_x
        for (int i = 0; i < N; i++) {
            delta_g = g[i] - g_old[i];
            delta_x = x_bar[i] - x[i];
            dot_delta_xg += delta_x*delta_g;
            dot_delta_xx += delta_x*delta_x;
        }
         
        // x_{ell+1} = x_bar
        std::memcpy(x, x_bar, sizeof(T)*N); 
         
        if ( dot_delta_xg <= 0 ) {
            alpha_ell = alphaMax;
        } else {
            // max{ alphaMin, (dot_delta_xx/dot_delta_xg) }
            alpha_ell = alphaMin > (dot_delta_xx/dot_delta_xg)? 
                        alphaMin : (dot_delta_xx/dot_delta_xg);
            // min
            alpha_ell = alpha_ell < alphaMax ? alpha_ell : alphaMax;
        }
         
        ell++; 
         
    }
    
    if (not verbose) {
        std::cerr << "\r";
    }

    if (hasConverged) {
        std::cout << "Has converged: ℓ: " << ell << std::endl;    
    } else {
        std::cout << "Did not converge: ℓ: " << ell << std::endl;    
    }

    delete [] g;
    delete [] r_bar;
    delete [] x_bar;
    delete [] g_old;
    
}


template void Spgl1Algorithm::LASSO_approx( FastTransformBase<double>& ft, double* b, double tau, double delta, double *x, double* r, int maxItr, bool verbose);


/*

Searches for the larges element in the r-vector. The vector is assumed to be 
non-negative. 

The search will start at the end of the vector and search through 
min{min1, min2} of the entries. It is assumed min1 > 0, min2 > 0.


*/
double Spgl1Algorithm::findMaxElementInVector(const std::vector<double>& r,const unsigned int min1, const unsigned int min2) {
    
    const unsigned int maxItr = (min1 < min2) ? min1 : min2;  
    
    if (maxItr <= 0) {
        std::cerr << "Error: findMaxElementInVector to few iterations"  << std::endl;    
        return r[r.size()-1];
    }
    
    const unsigned int r_size = r.size();
    
    double r_max = 0;
    if (maxItr > r_size) { // Check all elements
        for (unsigned int i = 0; i < r_size; i++) {
            if (r_max < r[i]) {
                r_max = r[i];    
            }
        }
    } else { // Check maxItr last elements
        for (unsigned int i = 0; i < maxItr; i++) {
            if (r_max < r[r_size - 1 - i]) {
                r_max = r[r_size - 1 - i];    
            }
        }
    }
    
    return r_max;
    
} 
    
    


/*

Projection of an N-vector onto the one-norm ball with radius tau.

Solves the optimization problem

P_τ[c] = argmin_{x} ||c-x||_2 subject to ||x||_1 ≤ τ


c   - Input vector of length N, which will be projected onto the one-norm 
      ball with norm tau. The resulting projection will be written directly 
      into c 
N   - Length of the vector
tau - Radius of one-norm ball
eps - Error threshold 

See. eq (4.1) in "Probing the Pareto Frontier For Basis Pursuit Solutions" by 
E. van den Berg and M. P. Friedlander for further information 

*/
template<typename T>
void Spgl1Algorithm::projectNorm1(T* c, const unsigned int N, const double tau, const double eps) {
    
    //std::cerr << "Project Real Called" << std::endl;
    
    // Create an vector of the absolute values of the c-vector
    std::vector<T> c_abs(N);
    
    // Find the 1 norm and insert all values in c_abs;
    T c_norm1 = 0;
    T abs_value;
    for (int i = 0; i < N; i++) {
        abs_value = std::abs(c[i]);
        c_abs[i] = abs_value;
        c_norm1 += abs_value; 
    }

    if (c_norm1 <= tau) {
        //std::cerr << "projectNorm1 terminates without any exicution" << std::endl;
        return;    
    }
    
    
    std::make_heap(c_abs.begin(), c_abs.end());
    
    // Make the first iteration outsize the loop.
    double local_delta = 0;
    double c_max = c_abs.front();
    double nu = c_max - tau;
    int    j = 1;
    double gamma = nu/j;

    while( j <= N and c_max > gamma) {

        // Remove the largest element from the heap
        std::pop_heap(c_abs.begin(), c_abs.end());
        c_abs.pop_back();
        
        // This is the value we are searching for
        local_delta = gamma;
        
        
        // prepare for next iteration
        j++;
        c_max = c_abs.front();
        nu += c_max;
        gamma = nu/j;
        
    }
    
    // Next we perform a soft threshold of the c-vector
    T max_value = 0;
    T value;
    for (int i = 0; i < N; i++) {
        value = std::abs(c[i]) - local_delta;
        max_value = (eps > value) ? 0: value;  
        
        if (c[i] < 0) {
            c[i] = -max_value;
        } else {
            c[i] = max_value;    
        }
    
    }
}

template void Spgl1Algorithm::projectNorm1<>(double* c, const unsigned int N, const double tau, const double eps); 



template<typename T>
void Spgl1Algorithm::projectNorm1(std::complex<T> *c, const unsigned int N, const double tau, const double eps) {
    
    //std::cerr << "Project Complex Called" << std::endl;
    
    // Create an vector of the absolute values of the c-vector
    T * c_modulus = new T[N];

    // Find the length of each number  
    double norm1 = 0; 
    T abs_value;
    for (int i = 0; i < N; i++) {
        abs_value = std::abs(c[i]);
        c_modulus[i] = abs_value;
        norm1 += abs_value;
    }
    
    projectNorm1<T>(c_modulus, N, tau);
    
    
    for (int i = 0; i < N; i++) {
        if ( c_modulus[i] > eps ) {
            c[i] = (c[i]/std::abs(c[i]))*c_modulus[i];
        } else {
            c[i] = 0;    
        }
    }

    
}




template void Spgl1Algorithm::projectNorm1<>(std::complex<double> *c, const unsigned int N, const double tau, const double eps);




