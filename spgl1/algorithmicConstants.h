#include <iostream>
#include <sstream>
#include <fstream>
#include <cstring>
#include <string>
#include <iomanip>
#include <vector>
#include <list>
#include <limits>
#include <cstdio>
#include <cmath>

#pragma once

/*

A class which read a configuration file containing various algorithmic constants
for the spgl1 algorithm. It is used so that one do not need to recompile the 
program each time one would like change some settings.

*/
class AlgorithmicConstants {
    
    protected: 
        double gamma;
        double alphaMax;
        double alphaMin;
        double delta;
        double tau;
        int lineSearchHistoryLength;
        int LASSOMaxItr; 
        double subsamplingFraction;
        double sigma;
        double threshold;
        int vm; // vanishing moments


    public:
         
        AlgorithmicConstants() : alphaMax(-1), alphaMin(-1), delta(-1),
                                 tau(-1), gamma(-1), sigma(-1), LASSOMaxItr(-1),
                                 threshold(-1), vm(-1), subsamplingFraction(-1){};
         
        bool readConstants(const char * filename);
        void printConstants(); 


        double getGamma()    { return gamma; }
        double getAlphaMax() { return alphaMax; }
        double getAlphaMin() { return alphaMin; }
        double getDelta()    { return delta; }
        double getTau()      { return tau; }
        double getSigma()    { return sigma; }
        double getThreshold()    { return threshold; }
        double getSubsamplingFraction()    { return subsamplingFraction; }
        int getVm() { return vm; } 
        int getLASSOMaxItr() { return LASSOMaxItr; } 
        int getLineSearchHistoryLength() { return lineSearchHistoryLength; }
         
};


















