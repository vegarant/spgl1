/*

This header file contain support function to the image program, so that the 
function calls in this program can be kept as high level as possible.

*/



#pragma once
#include "stdafx.h"
#include "supportFunctions.h"
#include <opencv2/opencv.hpp>
#include <chrono>       // std::chrono::system_clock
#include <algorithm>
#include <limits>
#include "../fastwht/hadamard.h"
#include "../wavelets/DWT.h"


using namespace cv;
enum HadamardOrder {ORDINARY, SEQUENCY, PALEY};

double * convertImageToVector(const cv::Mat & image );
cv::Mat  convertVectorToImage(double *image, const size_t M, const size_t N);

double sparsifyImage(double *image, const size_t N, const double threshold , int vm);

double* hadamardSampleImage(double * image, const size_t  N, const std::vector<int> idx,
                        HadamardOrder order);

std::vector<int> createSamplingPattern(const size_t N, const size_t M, 
                                       const double a, const double b);

void applyIDWT(double * image, const size_t N, const int vm);

std::vector<int> create_uniform_sampling_scheme(const size_t M, const size_t N);
std::vector<int> create_uniform_random_subsampling(std::vector<int> & scheme);


template<typename T> 
void IDWT2d(T* x, const int N , const int M, const Filter &DB_h, 
            const Filter &DB_g, const int nres);

template<typename T> 
void DWT2d(T* x, const int N , const int M, const Filter &DB_h, 
            const Filter &DB_g, const int nres);

template<typename T> 
void hadamard2d(T* x, const int N , const int M);

std::vector<int> create_rectangular_scheme(const unsigned int M, 
                                           const unsigned int N);

const std::vector<int> create_rect_random_subsampling(
                             const std::vector<int> & scheme);

void convertTo2dIdx(const int id, int& x, int& y, const int im_cols);
const int convertTo1dIdx(const int x, const int y, const int im_cols);
double* hadamardSampleImage2d(double * image, const size_t  N, 
                              const std::vector<int> idx);




