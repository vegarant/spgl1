#include "fastTransform.h"
#include "supportFunctions.h"






class Spgl1Algorithm {
     
    double alphaMin; // step length
    double alphaMax;
    //double delta;     // decent parameter, \delta \in (0,1)
    double gamma; // Decent parameter \in (0,1)
    int lineSearchHistoryLength;
     
    public:
        
    Spgl1Algorithm(double alphaMin, double alphaMax,  double gamma, 
                  int lineSearchHistoryLength):
                            alphaMin(alphaMin), alphaMax(alphaMax), gamma(gamma),
                            lineSearchHistoryLength(lineSearchHistoryLength) {};
   

    // Dummy constructor
    Spgl1Algorithm() {};

    template <typename T>
    T* rootFinding(FastTransformBase<T>& ft, T* b,  double sigma, int LASSOMaxItr);
     
     
     
    template <typename T>
    void LASSO_approx( FastTransformBase<T>& ft, T* b, double tau, double delta, T *x, T* r, int maxItr=400, bool verbose=false);
     
     
     
    template<typename T>
    void projectNorm1(T* c, const unsigned int N, const double tau, const double eps=1e-12);
     
     
    template<typename T>
    void projectNorm1(std::complex<T> *c, const unsigned int N, const double tau, const double eps = 1e-12);
 
    double findMaxElementInVector(const std::vector<double>& r,const unsigned int min1, const unsigned int min2);


};


