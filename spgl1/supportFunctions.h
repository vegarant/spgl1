
#pragma once
#include <complex>
#include <iostream>

/*

This header contains various support functions used by all the other algorithms.

*/


template <typename T>
double norm1(const T * x, const size_t N);

template <typename T>
double norm2 (const T *x, const size_t N);

template <typename T>
double normInfty(const T *x, const size_t N);


template <typename T>
double dot(const T *x, const T *y, const size_t N);

template <typename T>
std::complex<double> dot(const std::complex<T> *x, const std::complex<T> *y, const size_t N);

template <typename T>
void printArray(const T * x, const unsigned int N) {
    
    for (int i = 0; i < N; i++) {
        std::cout << x[i] << std::endl;
    }
    
}


template<typename T> 
void hadamard2d(T* x, const int N , const int M);
 
template <typename T>
void printArray(const std::complex<T> *x, const unsigned int N) {
    
    for (int i = 0; i < N; i++) {
        std::cout << x[i] << std::endl;
    }
    
}










