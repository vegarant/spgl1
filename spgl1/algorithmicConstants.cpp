#include "algorithmicConstants.h"


bool AlgorithmicConstants::readConstants(const char * filename) {
    std::ifstream infile(filename);
    std::string line;
    std::string variableName;
    double value;
    int valueInt;

    if (infile.is_open()) {
        
        while(std::getline(infile, line)) {
            
            if (line.length()  < 2) {
                continue;     
            }
            
            std::stringstream ss(line);
            std::getline(ss, variableName, '=');
            
            
            if (variableName.find("ALPHA_MIN") != std::string::npos) {
                
                ss >> value;
                alphaMin = value;
                
            } else if (variableName.find("ALPHA_MAX") != std::string::npos) {
                
                ss >> value;
                alphaMax = value;
            
            } else if (variableName.find("GAMMA") != std::string::npos) {
            
                ss >> value;
                gamma = value;    
            
            } else if (variableName.find("THRESHOLD") != std::string::npos) {
                 
                ss >> value;
                threshold = value;    
                 
            } else if (variableName.find("SIGMA") != std::string::npos) {
            
                ss >> value;
                sigma = value;    
            
            } else if (variableName.find("DELTA") != std::string::npos) {
                
                ss >> value;
                delta = value;
                
            } else if (variableName.find("SUBSAMPLING_FRACTION") != std::string::npos) {
                
                ss >> value;
                subsamplingFraction = value; 
                
            } else if (variableName.find("TAU") != std::string::npos) {
                
                ss >> value;
                tau = value; 
                
            } else if (variableName.find("VM") 
                                                        != std::string::npos) {
                ss >> valueInt;
                vm = valueInt;
            
            } else if (variableName.find("LINE_SEARCH_HISTORY_LENGTH") 
                                                        != std::string::npos) {
                ss >> valueInt;
                lineSearchHistoryLength = valueInt;
            
            } else if (variableName.find("LASSO_MAX_ITR") 
                                                        != std::string::npos) {
                ss >> valueInt;
                LASSOMaxItr = valueInt;
            }
        }
        
        infile.close();
    }
}

    
void AlgorithmicConstants::printConstants() {
    
    std::cout << "***************************** Algorithmic Constants ****************************"  << std::endl;
    std::cout << "alphaMin:                " << alphaMin << std::endl;
    std::cout << "alphaMax:                " << alphaMax << std::endl;
    std::cout << "delta:                   " << delta << std::endl;
    std::cout << "gamma:                   " << gamma << std::endl;
    std::cout << "sigma:                   " << sigma << std::endl;
    std::cout << "threshold:               " << threshold << std::endl;
    std::cout << "subsamplingFraction:     " << subsamplingFraction << std::endl;
    std::cout << "vm:                      " << vm << std::endl;
    std::cout << "LASSOMaxItr:             " << LASSOMaxItr << std::endl;
    std::cout << "lineSearchHistoryLength: " << lineSearchHistoryLength 
                                             << std::endl;   
    std::cout << "tau:                     " << tau << std::endl;
    char footer[81];
    std::memset(footer, '*', 80);
    footer[80] = '\0';
    std::cout << footer << std::endl;
    
    
    
    
    
    
    
    
}








