#include "DWT.h"
#include <iostream>
#include <iomanip>
#include <vector>
#include <cstdio>
#include <cmath>
#include <complex>
#include <cassert>
#include "../fastwht/hadamard.h"














//******************************************************************************
//******************************************************************************
//***                                                                        ***
//***                     All template related functions                     ***
//***                                                                        ***
//******************************************************************************
//******************************************************************************




template <typename T>
int DWTPeriodic(T * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres) {
    
    const int length_nres = int (round(log2(double(N)) + 1e-5) );
    if ( nres == 0 ) {
        return -1;    
    }
    
    if (length_nres < nres) {
        std::cerr << "Signal to short 2^" << length_nres << ", is not enough " <<
        "to resolve 2^" << nres << std::endl;
        assert(length_nres < nres);
    }
    
    
    size_t N_tmp = N;
    for (int res = 0; res < nres; res++) {
        
        DWTPeriodicOneRes<T>(x, N_tmp, DB_h, DB_g); 
        N_tmp /= 2;
        
    }
    
    return 0;
    
}


// Specialization
template int DWTPeriodic<>(double * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);
template int DWTPeriodic<>(float  * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);
template int DWTPeriodic<>(std::complex<double> * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);




template <typename T>
int IDWTPeriodic(T * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres) {
    
    const int length_nres = int (round(log2(double(N)) + 1e-5) );
    if ( nres == 0 ) {
        return -1;    
    }
    
    if (length_nres < nres) {
        std::cerr << "Signal to short 2^" << length_nres << ", is not enough " <<
        "to resolve 2^" << nres << std::endl;
        assert(length_nres < nres);
    }
    
    
    for (int res = nres; res > 0; res--) {
    
        // Find length of the first detail space 
        int length = int(pow(2, length_nres - (res-1) )+1e-4);   
        
        
        IDWTPeriodicOneRes<T>(x, length, DB_h, DB_g); 
        //std::cout << "-------------- res: " << res << " ----------------" << std::endl;
        //for (int i = 0; i < length; i++) {
        //    std::cout << x[i] << std::endl;
        //}
    }
    
    return 0;
    
}



// Specialization
template int IDWTPeriodic<>(double * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);
template int IDWTPeriodic<>(float * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);
template int IDWTPeriodic<>(std::complex<double> * x, const size_t N, const Filter &DB_h, const Filter &DB_g, const int nres);



/*
Input:
x   - Signal
N   - Signal length
DB_h   - Daubechies h-filter
DB_g   - Daubechies g-filter
*/
template <typename T>
void DWTPeriodicOneRes(T * x, const size_t N, const Filter &DB_h, const Filter &DB_g) {
    
    const size_t N_h = DB_h.size();
    const size_t N_g = DB_g.size();
    
    if ((N % 2) != 0) {
        std::cerr << "Error: signal length is odd\n";    
    }
    
    const int start_idx_h = DB_h.start_idx();
    const int start_idx_g = DB_g.start_idx();
    const int end_idx_h = DB_h.end_idx();
    const int end_idx_g = DB_g.end_idx();
    
    const int end_idx = end_idx_h > end_idx_g ? end_idx_h : end_idx_g;
    const int start_idx = start_idx_h < start_idx_g ? start_idx_h : start_idx_g;
    
    int p, n, idx_x; 
    T *out = new T[N];
    T sumH, sumG;
    
    for (p = 0; p < N/2; p++) {
        
        sumH = 0;
        sumG = 0;
        for (n = start_idx + 2*p; n <= end_idx + 2*p; n++) {
            idx_x = mapToIntervalPeriodic(n,0,N);
            
            sumH += DB_h[n-2*p]*x[idx_x];    
            sumG += DB_g[n-2*p]*x[idx_x];    
        }
        
        //std::cout << sumH << std::endl;
        out[p]     = sumH;
        out[p+N/2] = sumG;
        
    }
     
    std::memcpy(x, out, sizeof(T)*N);
    delete [] out;
}    



template void DWTPeriodicOneRes<>(double * x, const size_t N, const Filter &DB_h, const Filter &DB_g);
template void DWTPeriodicOneRes<>(float * x, const size_t N, const Filter &DB_h, const Filter &DB_g);
template void DWTPeriodicOneRes<>(std::complex<double> * x, const size_t N, const Filter &DB_h, const Filter &DB_g);




/*
Input:
x   - Signal
N   - Signal length
h   - Daubechies h-filter
g   - Daubechies g-filter
*/
template <typename T>
void IDWTPeriodicOneRes(T * x, const size_t N, const Filter & DB_h, const Filter &DB_g) {
    
    size_t N_h = DB_h.size();
    size_t N_g = DB_g.size();
    
    if ((N % 2) != 0) {
        std::cerr << "Error: signal length is odd\n";    
    }
     
    const int start_idx_h = DB_h.start_idx();
    const int start_idx_g = DB_g.start_idx();
    const int end_idx_h = DB_h.end_idx();
    const int end_idx_g = DB_g.end_idx();
    
    const int end_idx = end_idx_h > end_idx_g ? end_idx_h : end_idx_g;
    const int start_idx = start_idx_h < start_idx_g ? start_idx_h : start_idx_g;
    
    int p, n, idx_a, idx_d;
    
    T *out = new T[N];
    T sum;
    int cnt = 0;
    
    for (p = 0; p < N; p++ ) {
         
        sum = 0;
        int n_start = ((p-end_idx)/2) -1;
        int n_end   = ((p-start_idx)/2)+1;

        for (n = n_start; n <= n_end; n++) {
            
            idx_a = mapToIntervalPeriodic(n,0,N/2);
            idx_d = mapToIntervalPeriodic(n,N/2, N);
            
            sum += x[idx_a]*DB_h[p-2*n];
            sum += x[idx_d]*DB_g[p-2*n];
        }
        
        out[p] = sum;
        
    }
    
    std::memcpy(x, out, sizeof(T)*N);
    delete [] out;   
}


template void IDWTPeriodicOneRes<>(double * x, const size_t N, const Filter & DB_h, const Filter &DB_g);
template void IDWTPeriodicOneRes<>(float  * x, const size_t N, const Filter & DB_h, const Filter &DB_g);
template void IDWTPeriodicOneRes<>(std::complex<double> * x, const size_t N, const Filter & DB_h, const Filter &DB_g);


// This is by no means a good implementation but it works
/*
Maps the idx into the interval [a, b) using a periodic maping
*/
int mapToIntervalPeriodic(int idx, const int a, const int b) {

    const int N = b-a;
    const int factor = std::abs((idx-a)/N);
    //std::cout << "factor: " << factor << std::endl;
    if (std::abs((idx-a) % N)) {  
        if (idx < a) {
            idx = idx + (factor+1)*N;
        } else if ( idx >= b ) {
            idx =  a + (idx-b)%N;
        }
    } else {
        idx = a;
    }
    return idx;
}


/*******************************************************************************
***                                                                          ***
***                The first version of the wavelet library                  ***
***                                                                          ***
*******************************************************************************/


std::vector<double> DWTPeriodic(std::vector<double> & x, const Filter &h, const Filter &g, int nres) {
     
    const int N = x.size();
     
    int length_nres = int (round(log2(double(N)) + 1e-5) );
    if ( nres == 0 ) {
        return x;    
    }
     
    if (length_nres < nres) {
        std::cerr << "Signal to short 2^" << length_nres << ", is not enough " <<
        "to resolve 2^" << nres << std::endl;
        assert(length_nres < nres);
    }
     
    // Split the vector into a low resolution and a detail space
    std::vector<double> x_dwt = DWTPeriodicOneRes(x,h,g); 
     
    // Slice out the low resolution part
    std::vector<double> v(x_dwt.begin(), x_dwt.begin()+N/2);
     
    // Make a new wavelet wavelet transform on the low resolution part 
    std::vector<double> out = DWTPeriodic(v ,h, g, nres-1);
     
    // Copy the newly transformed signal into the output vector 
    // This is not a good solution, but it works
    for (int i = 0; i < N/2; i++) {
        x_dwt[i] = out[i];
    }
     
    return x_dwt;
    
}


std::vector<double> IDWTPeriodic(std::vector<double> & x, const Filter &h, const Filter &g, int nres) {
    
    const int N = x.size();
    
    int length_nres = int (round(log2(double(N)) + 1e-5) );
    //std::cout << "length_nres: " << length_nres << std::endl;

    if (length_nres < nres) {
        std::cerr << "Signal to short 2^" << length_nres << ", is not enough " <<
        "to resolve 2^" << nres << std::endl;
        assert(length_nres < nres);
    }
    
    // Copy x
    std::vector<double> out = x;
    
    for (nres; nres > 0; nres--) {
        // Find length of the first detail space 
        int length = int(pow(2, length_nres - (nres-1) )+1e-4);   
        
        //std::cout << "nres: " << nres  << std::endl;
        //std::cout << "length: " << length << std::endl;
        
        std::vector<double> v(out.begin(), out.begin() + length);
        std::vector<double> x_idwt = IDWTPeriodicOneRes(v, h, g);
        //std::cout << "x_idwt.size()" <<x_idwt.size() << std::endl;

        for (int i = 0; i < length; i++) {
            out[i] = x_idwt[i];
        }

    }
    
    return out;
    
}


std::vector<double> DWTPeriodicOneRes(std::vector<double> & x, const Filter &h, const Filter &g) {
    
    size_t N = x.size();
    size_t N_h = h.size();
    size_t N_g = g.size();
    
    if ((N % 2) != 0) {
        std::cerr << "Error: signal length is odd\n";    
    }
    
    const int start_idx_h = h.start_idx();
    const int start_idx_g = g.start_idx();
    const int end_idx_h = h.end_idx();
    const int end_idx_g = g.end_idx();
    
    const int end_idx = end_idx_h > end_idx_g ? end_idx_h : end_idx_g;
    const int start_idx = start_idx_h < start_idx_g ? start_idx_h : start_idx_g;
    
    //std::cout << "DWT: start_idx: " << start_idx << std::endl;
    //std::cout << "DWT: end_idx: " << end_idx << std::endl;
    std::vector<double> out(N);
    int p, n, idx_x; 
    double sumH, sumG;
    //int cnt = 0;
    for (p = 0; p < N/2; p++) {
        
        sumH = 0;
        sumG = 0;
        for (n = start_idx + 2*p; n <= end_idx + 2*p; n++) {
            idx_x = mapToIntervalPeriodic(n,0,N);
            
            //std::cout << "DWT: n - 2*p: " << n - 2*p << std::endl;
            //if (start_idx <= n - 2*p && n - 2*p <= end_idx ) {
            //    cnt++;    
            //}
            sumH += h[n-2*p]*x[idx_x];    
            sumG += g[n-2*p]*x[idx_x];    
        }
        
        out[p]     = sumH;
        out[p+N/2] = sumG;
        
    }
    //std::cout << "DWT: cnt: " << cnt << std::endl;   

    return out;
    
}

std::vector<double> IDWTPeriodicOneRes(std::vector<double> & x, const Filter & h, const Filter &g) {
    
    size_t N = x.size();
    size_t N_h = h.size();
    size_t N_g = g.size();
    
    if ((N % 2) != 0) {
        std::cerr << "Error: signal length is odd\n";    
    }
     
    std::vector<double> out(N);
    const int start_idx_h = h.start_idx();
    const int start_idx_g = g.start_idx();
    const int end_idx_h = h.end_idx();
    const int end_idx_g = g.end_idx();
    
    const int end_idx = end_idx_h > end_idx_g ? end_idx_h : end_idx_g;
    const int start_idx = start_idx_h < start_idx_g ? start_idx_h : start_idx_g;
    
    int p, n, idx_a, idx_d;
    double sum;
    int cnt = 0;
    for (p = 0; p < N; p++ ) {
         
        sum = 0;
        int n_start = ((p-end_idx)/2) -1;
        int n_end   = ((p-start_idx)/2)+1;

        for (n = n_start; n <= n_end; n++) {
            
            idx_a = mapToIntervalPeriodic(n,0,N/2);
            idx_d = mapToIntervalPeriodic(n,N/2, N);
            
            sum += x[idx_a]*h[p-2*n];
            sum += x[idx_d]*g[p-2*n];
        }
        
        out[p] = sum;
        
    }
    
    return out;
    
}

